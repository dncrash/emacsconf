;;; notify_script.el

;; requires org-ql: https://github.com/alphapapa/org-ql
;; this is largely based on the org-ql example: https://github.com/alphapapa/org-ql/blob/master/examples/org-bills-due.el

;;;; Initialize package system

(require 'package)
;;(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
;;			 ("melpa" . "https://melpa.org/packages/")))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; Add org-ql sources to load-path
(setq load-path (append (list "~/builds/org-ql"
                              "~/.config/emacs/load-path")
                        load-path))

;;;; Load packages
;; Built-in
(require 'cl-lib)
(require 'subr-x)
(require 'org)
(require 'org-habit)
(require 'org-agenda)
;; From MELPA
(require 's)
(require 'dash)
;; (require 'dash-functional)
;; org-ql
(require 'org-ql)

;;;; Main

;; items scheduled in the next 3 days or older
(-if-let* ((scheduled_header "Scheduled within the next 3 days")
           (scheduled_items (org-ql-query
                              :from (org-agenda-files)
                              :where '(and (todo "TODO")
                                          (tags "notify"))
                              :select '(org-get-heading t t)))
           (scheduled_string (concat "<ul>"
                                     (s-join "\n"
                                             (--map (concat "<li>" it "</li>")
                                                    scheduled_items))
                                     "</ul>")))

    (progn
      ;; Send notification
      (call-process "notify-send" nil 0 nil
                    scheduled_header
                    scheduled_string)
      ;; Also output to STDOUT
      (princ (format "%s:\n%s" scheduled_header scheduled_string)))
    ;; No notifications
    ;; print asdfasdfasdfadf
    )

;; items with deadlines for today or older
(-if-let* ((deadline_header "Deadlines Today")
           (deadline_items (org-ql-query
                             :from (org-agenda-files)
                             :where '(and (deadline <= today)
                                          (todo "TODO")
                                          (tags "notify"))
                             :select '(org-get-heading t t)))
           (deadline_string (concat "<ul>"
                                    (s-join "\n"
                                            (--map (concat "<li>" it "</li>")
                                                   deadline_items))
                                    "</ul>")))
    (progn
      ;; Send notification
      (call-process "notify-send" nil 0 nil
                    deadline_header
                    deadline_string)
      ;; Also output to STDOUT
      (princ (format "\n%s:\n%s" deadline_header deadline_string))
      ;; Also output to kdeconnect; OnePlus device id = 3e1c084f71757d44
      (shell-command (format "kdeconnect-cli --ping-msg '%s' -d 4bc6f9d560ba3745" deadline_items)))

  ;; No notifications
  (princ "no deadlines")
  (kill-emacs 1))

(provide 'notfiy_script.el)
;;; notify_script.el ends here
