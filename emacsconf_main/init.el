;; init.el  --- Custom init file
;;; Commentary:
;; custom init file

;; style guide: https://github.com/bbatsov/emacs-lisp-style-guide

;;; Code:

;; shut up about symlinks
(setq vc-follow-symlinks t)

;; elpaca packet manager
(defvar elpaca-installer-version 0.9)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1 :inherit ignore
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let* ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                  ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                  ,@(when-let* ((depth (plist-get order :depth)))
                                                      (list (format "--depth=%d" depth) "--no-single-branch"))
                                                  ,(plist-get order :repo) ,repo))))
                  ((zerop (call-process "git" nil buffer t "checkout"
                                        (or (plist-get order :ref) "--"))))
                  (emacs (concat invocation-directory invocation-name))
                  ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                        "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                  ((require 'elpaca))
                  ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))
; disable package.el in favor of elpaca
(setq package-enable-at-startup nil)

;; Load settings.org
(setq custom-file (concat user-emacs-directory "settings.org"))
(when (file-exists-p custom-file)
  (org-babel-load-file custom-file))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#c5c8c6" "#cc6666" "#b5bd68" "#f0c674" "#81a2be" "#b294bb" "#8abeb7" "#373b41"))
 '(auto-image-file-mode t)
 '(custom-safe-themes
   (quote
    ("82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "938d8c186c4cb9ec4a8d8bc159285e0d0f07bad46edf20aa469a89d0d2a586ea" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" default)))
 '(dired-hide-details-hide-information-lines nil)
 '(diredp-hide-details-initially-flag nil)
 '(display-time-mode t)
 '(fci-rule-color "#373b41")
 '(flycheck-color-mode-line-face-to-color (quote mode-line-buffer-id))
 '(hl-sexp-background-color "#efebe9")
 '(image-animate-loop t)
 '(indent-tabs-mode nil)
 '(nyan-animate-nyancat t)
 '(org-agenda-include-diary t)
 '(package-selected-packages
   (quote
    (dired-rainbow exec-path-from-shell expand-region idle-highlight-mode popwin prodigy smartparens use-package yasnippet ox-reveal multiple-cursors keychain-environment org web-mode smex yaml-mode color-theme counsel-projectile which-key jedi color-theme-sanityinc-tomorrow swiper lorem-ipsum try tldr org-alert nyan-mode magit dired+)))
 '(require-final-newline t)
 '(show-paren-mode t)
 '(tab-width 4)
 '(electric-indent-mode -1)
 '(tool-bar-mode nil)
 '(scroll-bar-mode nil)
 '(tramp-default-method "sshx" nil (tramp))
 '(vc-annotate-background nil)
 '(vc-annotate-very-old-color nil)
 '(visible-bell t)
 '(visible-cursor nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "simp" :slant normal :weight normal :height 83 :width normal))))
 '(org-block ((t (:inherit default))))
 '(org-meta-line ((t (:inherit default :foreground "#718c00")))))

(put 'erase-buffer 'disabled nil)
;;; init.el ends here
