* Config files
#+NAME: create symlinks
#+BEGIN_SRC sh :results replace drawer :tangle install.sh :shebang "#!/bin/bash"
  symlinks() {
      files_path="$(pwd)/emacsconf_main"
      mkdir -p "$HOME"/.config/emacs
      if ln -sf "$files_path"/*.el "$HOME"/.config/emacs/ && \
          ln -sf "$files_path"/settings.org "$HOME"/.config/emacs/settings.org
      then
          echo 'Symlinks done!'
      else
          echo 'Symlinks failed!'
      fi
  }
#+END_SRC

#+NAME: mail settings
#+BEGIN_SRC sh :results replace drawer :tangle install.sh
  mail() {
      git submodule init
      ln -sf "$(pwd)"/emacsconf_mail/mail_conf.el "$HOME"/.config/emacs/
  }
#+END_SRC

* Systemd units
** Emacs
#+NAME: symlink emacs systemd unit
#+BEGIN_SRC sh :results replace drawer :tangle install.sh
  unit_files() {
      if systemctl --user | grep 'emacs' &>/dev/null; then
      echo 'Found "emacs" in systemd units. Skipping.'
      else
      mkdir -p "$HOME"/.config/systemd/user
      ln -sf "$(pwd)"/service_files/emacs.service "$HOME"/.config/systemd/user/emacs.service
      systemctl --user enable emacs
      if systemctl --user start emacs; then
          echo 'Started emacs daemon.'
      else
          echo 'Error starting emacs daemon.'
      fi
      fi
  }
#+END_SRC
** Notifications
#+NAME: symlink notifications service & timer
#+BEGIN_SRC sh :results replace drawer :tangle install.sh
  notifications() {
    if systemctl --user | grep 'notifications' &>/dev/null; then
      echo 'Found "notifications" in systemd units. Skipping.'
    else
      ln -sf "$(pwd)"/service_files/notifications.service "$HOME"/.config/systemd/user/notifications.service
      ln -sf "$(pwd)"/service_files/notifications.timer "$HOME"/.config/systemd/user/notifications.timer
      ln -sf "$(pwd)"/emacsconf_main/notify_script.el "$HOME"/.config/emacs/notify_script.el
      systemctl --user daemon-reload
      systemctl --user enable notifications.timer
      if systemctl --user start notifications.timer; then
          echo 'Started notifications daemon.'
      else
          echo 'Error starting notifications daemon.'
      fi
    fi
  }
#+END_SRC

* Options parser
#+BEGIN_SRC sh :results replace drawer :tangle install.sh
  full() {
      symlinks
      mail
      unit_files
      notifications
  }

  help() {
      echo "Usage: $0 (symlinks | full)"
  }

  if [[ "$#" == 1 && ( "$1" == 'symlinks' || "$1" == 'full' ) ]]; then
      "$1"
  else
      help && exit 2
  fi

#+END_SRC
* Start client instance by default
#+BEGIN_SRC bash
  cp /usr/share/applications/emacs.desktop ~/.local/share/applications/emacs.desktop
  sed 's/Exec=.*/Exec=emacsclient -c -a \"emacs\" %F/' -i ~/.local/share/applications/emacs.desktop
#+END_SRC
* Usage
- To eval individual code blocks: C-c C-c
- To eval elisp below: C-x C-e

- eval this to remove output (C-c C-v k)
(org-babel-remove-result-one-or-many 1)

- eval this to output/export just the code bocks (C-c C-v t)
(org-babel-tangle)
