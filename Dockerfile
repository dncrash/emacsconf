FROM archlinux/base:latest

RUN pacman -Sy && pacman -S --noconfirm emacs git

COPY . /tmp

# RUN git clone https://gitlab.com/dncrash/emacsconf.git /root/emacsconf && \
RUN cd /tmp && \
    emacs --batch -l org install.org --eval "(require 'org)" --eval '(org-babel-tangle "install.org")' && \
    bash install.sh symlinks && \
    mkdir -p /root/org && \
    touch /root/org/diary


CMD ["bash", "-c", "emacs; /bin/bash"]
